#ifndef CONSTANTS_H
#define CONSTANTS_H

#define Aquila_TEST_TXTFILE "/Users/dany/Documents/c/tirocinio/aquila/aquila-master/tests/data/test.txt"

#define Aquila_TEST_PCMFILE "/Users/dany/Documents/c/tirocinio/aquila/aquila-master/tests/data/test_16b.dat"

#define Aquila_TEST_WAVEFILE_8B_MONO "/Users/dany/Documents/c/tirocinio/aquila/aquila-master/tests/data/8b_mono.wav"
#define Aquila_TEST_WAVEFILE_8B_STEREO "/Users/dany/Documents/c/tirocinio/aquila/aquila-master/tests/data/8b_stereo.wav"
#define Aquila_TEST_WAVEFILE_16B_MONO "/Users/dany/Documents/c/tirocinio/aquila/aquila-master/tests/data/16b_mono.wav"
#define Aquila_TEST_WAVEFILE_16B_STEREO "/Users/dany/Documents/c/tirocinio/aquila/aquila-master/tests/data/16b_stereo.wav"

#define Aquila_TEST_TXTFILE_OUTPUT "/Users/dany/Documents/c/tirocinio/aquila/tests/test_output.txt"
#define Aquila_TEST_PCMFILE_OUTPUT "/Users/dany/Documents/c/tirocinio/aquila/tests/test_output.dat"
#define Aquila_TEST_WAVEFILE_OUTPUT "/Users/dany/Documents/c/tirocinio/aquila/tests/test_output.wav"

#endif // CONSTANTS_H
