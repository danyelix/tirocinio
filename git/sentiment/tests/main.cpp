//
//  main.cpp
//  tests
//
//  Created by Daniele Galletti on 02/01/18.
//  Copyright © 2018 Daniele Galletti. All rights reserved.
//

#include <iostream>
#include "UnitTestPP.h"
#include "compare.h"

int main(int, char const *[])
{
    return UnitTest::RunAllTests();
}

/*extern "C" {
         // C linkage
    void delta_fisso(fvec_t* i,fvec_t* c,fvec_t* ris);
}*/

TEST(test_compare_delta_fisso){
    fvec_t* vett=new_fvec(10);
    for (int i=0; i<vett->length; i++) {
        fvec_set_sample(vett, i, i);
    }
   
    
    fvec_t* vett2=new_fvec(10);
    for (int i=0; i<vett2->length; i++) {
        fvec_set_sample(vett2, i, i);
    }
    fvec_t* ris=new_fvec(10);
    delta_fisso(vett, vett2, ris);
    for (int i=0; i<vett->length; i++) {
        CHECK_EQUAL(fvec_get_sample(ris, i), fvec_get_sample(vett, i));
    }
    del_fvec(vett);
    del_fvec(vett2);
    
}
