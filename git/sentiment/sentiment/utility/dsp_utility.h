//
//  dsp_utility.h
//  sentiment
//
//  Created by Daniele Galletti on 29/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef dsp_utility_h
#define dsp_utility_h

#include <stdio.h>
#include <stdlib.h>
#include "aubio.h"
#include "fvec.h"
#include "io/source.h"
#include <math.h>

#define TRUE 1
#define FALSE 0

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))


//************************** PRINT FUNCTIONS ***********************
/**
 Print a fvec with time that goes from 0, incrased of deltaTime everytime
 \param i fvec to print
 \param deltaTime time between two adiacent values of i, usually is equals to hop_size/samplerate
 **/
void print_fvec_with_time(fvec_t* i,float deltaTime);

//************************** COMPARISON FUNCTIONS *********************
/**
 Take a number and a vector to scan and return a vector of quadratic distances where i-th element of the returned array is the quadratic distance between the number and the i-th element of toScanner
 \param t number
 \param toScanner vector
 \return fvec of distances
 **/
fvec_t* fvec_distanzaQuadratica(smpl_t t,fvec_t* toScanner);
/**
 Calculate quadratic distances like fvec_distanzaQuadratica but if it find a 0 the function set 0 in that position and not the distance
 **/
fvec_t* fvec_distanzaQuadratica_noZero(smpl_t t,fvec_t* toScanner);
/**
 Take an array of mfcc (usually 12) and return a fmat of distances where every i-th element of t is compared with every (j,i) element of the matrix (j=0..height)
 /param t array of smpl_t
 /param toScanner fmat with which it must perform the distance
 /return fmat of distances
 **/
fmat_t* fmat_distanzaQuadratica(smpl_t* t,fmat_t* toScanner);
/**
 Calculate quadratic distances like fmat_distanzaQuadratica but if it find a jump (in aubio usually there is a jump value of -263) the function set 0 in that position and not the distance
 **/
fmat_t* fmat_distanzaQuadratica_noJump(smpl_t* t,fmat_t* toScanner);

/**
 Take a fvec and divides all for the highest sample, than multiplies for 100
 \param input fvec to normalize
 \return normalized fvec between 0 and 100
 **/
fvec_t* normalize(fvec_t* input);

/**
 
 **/
smpl_t calculate_sliding_distance(fvec_t* t, fvec_t* r);

smpl_t calculate_sliding_distance_fmat(fmat_t* t, fmat_t* r);
#endif /* dsp_utility_h */
