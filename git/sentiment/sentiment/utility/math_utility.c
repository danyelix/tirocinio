//
//  math_utility.c
//  sentiment
//
//  Created by Daniele Galletti on 12/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "math_utility.h"

float fvec_avg(fvec_t* t){
    if (t==NULL) {
        return 0;
    }
    int i;
    double tempSum=0;
    for (i=0; i<t->length; i++) {
        tempSum+=fvec_get_sample(t, i);
    }
    return tempSum/t->length;
}

float smpl_avg(smpl_t* t,size_t length){
    int i;
    double tempSum=0;
    for (i=0; i<length; i++) {
        tempSum+=t[i];
    }
    return tempSum/length;
}

void fmat_avg_fmat(fmat_t* t,fmat_t* store_here,uint_t channel){
    uint_t i,j;
    double temp_sum;
    for (j=0; j<t->length/*12*/; j++) {
        temp_sum=0;
        for (i=0; i<t->height; i++) {
            temp_sum+=fmat_get_sample(t, i, j);
        }
        fmat_set_sample(store_here, temp_sum/t->height, channel, j);
    }
}

fvec_t* getWin(fvec_t* t, uint_t win_size, uint_t start){
    fvec_t* nuovo=new_fvec(win_size);
    uint_t i;
    for (i=0; i<win_size; i++) {
        fvec_set_sample(nuovo, fvec_get_sample(t, i+start), i);
    }
    return nuovo;
}

uint_t min(uint_t a,uint_t b,uint_t c){
    if (a<b) {
        if (a<c) {
            return a;
        }else{
            return c;
        }
    }else if (b<c){
        return b;
    }else{
        return c;
    }
}

float fvec_min(fvec_t* t){
    float min=fvec_get_sample(t, 0);
    for (int i=1; i<t->length; i++) {
        if(min>fvec_get_sample(t, i)){
            min=fvec_get_sample(t, i);
        }
    }
    return min;
}

uint_t fvec_indice_min(fvec_t* t){
    float min=fvec_get_sample(t, 0);
    uint_t min_ind=0;
    for (int i=1; i<t->length; i++) {
        if(min>fvec_get_sample(t, i)){
            min=fvec_get_sample(t, i);
            min_ind=i;
        }
    }
    return min_ind;
}
