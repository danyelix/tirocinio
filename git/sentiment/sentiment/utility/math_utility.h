//
//  math_utility.h
//  sentiment
//
//  Created by Daniele Galletti on 12/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef math_utility_h
#define math_utility_h

#include <stdio.h>
#include "aubio.h"

float fvec_min(fvec_t* t);
float fvec_avg(fvec_t* t);
float smpl_avg(smpl_t* t,size_t length);
void fmat_avg_fmat(fmat_t* t,fmat_t* store_here,uint_t channel);
fvec_t* getWin(fvec_t* t, uint_t win_size, uint_t start);
uint_t min(uint_t a,uint_t b,uint_t c);
uint_t fvec_indice_min(fvec_t* t);
#endif /* math_utility_h */
