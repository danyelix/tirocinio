//
//  dsp_utility.c
//  sentiment
//
//  Created by Daniele Galletti on 29/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "dsp_utility.h"


void print_fvec_with_time(fvec_t* i,float deltaTime){
    int j;
    float time=0;
    for (j=0; j<i->length; j++) {
        printf("time: %f    %f\n",time,fvec_get_sample(i, j));
        time+=deltaTime;
    }
}

smpl_t calculate_sliding_distance_fmat(fmat_t* t, fmat_t* r){
    
    if(r==NULL ||t==NULL || t->data==NULL || r->data==NULL || t->height==0 || r->height==0){
        return 0;
    }
    
    int y1= -t->length, y2= 0;
    int x1=0,x2=r->length;
    int lb,ub,count;
    double temp,min_difference=0;
    
    while(y1<x2){
        
        ub= MIN(y2,x2);
        lb= MAX(y1,x1);
        if(ub-lb<0){
            break;
        }
        temp=0;
        count=0;
        for (int i=0; i<ub-lb; i++) {
            for (int j=0; j<12; j++) {
                temp+=pow(fmat_get_sample(r, lb+i, j)-fmat_get_sample(t, lb-y1+i, j), 2);
            }
            count++;
        }
        
        //inizializza min_difference
        if (min_difference==0 && temp!= 0) {
            min_difference=temp;
        }
        
        
        if(temp>0 && temp/count<min_difference){
            min_difference= temp/count;
        }
        y1++; y2++;
    }
    return min_difference;
}

smpl_t calculate_sliding_distance(fvec_t* t, fvec_t* r){
    
    if(r==NULL ||t==NULL || t->data==NULL || r->data==NULL || t->length==0 || r->length==0){
        return 0;
    }
    
    int y1= -t->length, y2= 0;
    int x1=0,x2=r->length;
    int lb,ub,count;
    double temp,min_difference=0;
    
    while(y1<x2){
        
        ub= MIN(y2,x2);
        lb= MAX(y1,x1);
        if(ub-lb<0){
            break;
        }
        temp=0;
        count=0;
        for (int i=0; i<ub-lb; i++) {
            temp+= pow(fvec_get_sample(r, lb+i)-fvec_get_sample(t, lb-y1+i), 2);
//            if (fvec_get_sample(r, lb+i)-fvec_get_sample(t, lb-y1+i) !=0) {
//                count++;
//            }
            count++;
        }
        
        //inizializza min_difference
        if (min_difference==0 && temp!= 0) {
            min_difference=temp;
        }
        
        
        if(temp>0 && temp/count<min_difference){
            min_difference= temp/count;
        }
        y1++; y2++;
    }
    return min_difference;
}

fvec_t* fvec_distanzaQuadratica(smpl_t t,fvec_t* toScanner){
    int i;
    fvec_t* ris=new_fvec(toScanner->length);
    for (i=0; i<toScanner->length; i++) {
        fvec_set_sample(ris, pow(fvec_get_sample(toScanner, i)-t,2), i);
    }
    return ris;
}

fmat_t* fmat_distanzaQuadratica(smpl_t* t,fmat_t* toScanner){
    int i,j;
    fmat_t* ris=new_fmat(toScanner->height, toScanner->length);
    for (i=0; i<toScanner->height; i++) {
        //dovrebbe iterare 12 volte
        for (j=0; j<toScanner->length; j++) {
            fmat_set_sample(ris, pow(fmat_get_sample(ris, i, j)-t[j],2),i, j);
        }
    }
    return ris;
}

fvec_t* fvec_distanzaQuadratica_noZero(smpl_t t,fvec_t* toScanner){
    int i;
    fvec_t* ris=new_fvec(toScanner->length);
    for (i=0; i<toScanner->length; i++) {
        if (fvec_get_sample(toScanner, i)==0) {
            fvec_set_sample(ris, 0, i);
        }else{
            fvec_set_sample(ris, pow(fvec_get_sample(toScanner, i)-t,2), i);
        }
    }
    return ris;
}

fmat_t* fmat_distanzaQuadratica_noJump(smpl_t* t,fmat_t* toScanner){
    int i,j;
    //printf("input matrix:\n");
    //fmat_print(toScanner);
    //printf("****************\n\n");
    fmat_t* ris=new_fmat(toScanner->height, toScanner->length);
    fmat_zeros(ris);
    for (i=0; i<toScanner->height; i++) {
        //controllo di salto del primo elemento, se salta oltre la soglia tutta la colonna viene azzerata
        if (fmat_get_sample(toScanner, i, 0)==(-263.7279358)) {
            continue;
        }
        //dovrebbe iterare 12 volte
        for (j=0; j<toScanner->length/*12*/; j++) {
            fmat_set_sample(ris, pow(fmat_get_sample(toScanner, i, j)-t[j],2),i, j);
        }
        //fmat_print(ris);
    }
    return ris;
}

fvec_t* normalize(fvec_t* input){
    uint_t i;
    float coeffMul, max=fvec_get_sample(input, 0);
    for (i=1; i<input->length; i++) {
        if (fvec_get_sample(input, i)>max) {
            max=fvec_get_sample(input, i);
        }
    }
    coeffMul=100/max;
    for (i=0; i<input->length; i++) {
        fvec_set_sample(input, fvec_get_sample(input, i)*coeffMul, i);
    }
    return input;
}
