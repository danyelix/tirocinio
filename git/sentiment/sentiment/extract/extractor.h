//
//  extractor.h
//  sentiment
//
//  Created by Daniele Galletti on 21/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef extractor_h
#define extractor_h

#include <stdio.h>
#include "aubio.h"
#include "fvec.h"
#include "io/source.h"
#include "math_utility.h"

/**
 Get a fvec of pitches calculated from all the hopses done
 \param s source file made with new_aubio_source
 \param hop_size size of the hopses in the pitch calculation (usually the same of hop_size in new_aubio_source)
 \return fvec of pitches
 **/
fvec_t* getPitch(aubio_source_t* s,int hop_size);
/**
 Get a fmat (a matrix) in which all mfcc are stored for each hop done. By default number of coefficents are 12
 \param s source file made with new_aubio_source
 \param hop_size size of the hopses (usually the same of hop_size in new_aubio_source)
 \return fmat of mfcc
 **/
fmat_t* getMfcc(aubio_source_t* s,int hop_size);
/**
 Get a fvec of energies from all the hopses done
 \param s source file made with new_aubio_source
 \param hop_size size of the hopses (usually the same of hop_size in new_aubio_source)
 \return fvec of pitches
 **/
fvec_t* getEnergy(aubio_source_t* s,int hop_size);

fvec_t* getPitch_fvec(fvec_t* src, uint_t win_size, uint_t start, uint_t hop_size,uint_t samplerate);

#endif /* extractor_h */
