//
//  extractor.c
//  sentiment
//
//  Created by Daniele Galletti on 21/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "extractor.h"

fvec_t* getPitch(aubio_source_t* s,int hop_size){
    fvec_t *in,*out;
    int i=0,win_size=2048;
    u_int read=0;
    
    //inizializza il pitch
    aubio_pitch_t* pitch=new_aubio_pitch("default", win_size, hop_size, aubio_source_get_samplerate(s));
    aubio_pitch_set_tolerance(pitch, 0.8);
    //vec di lettura della sorgente
    in=new_fvec(hop_size);
    //vec di pitch
    out=new_fvec(1);
    fvec_t* res=new_fvec(aubio_source_get_duration(s)/hop_size);
    //fvec_t* res_time=new_fvec(aubio_source_get_duration(s)/hop_size);
    do{
        aubio_source_do(s, in, &read);
        
        
        
        aubio_pitch_do(pitch, in, out);
        //printf("time: %f\t%f\n",((float)(i*hop_size))/((float)aubio_source_get_samplerate(s)),fvec_get_sample(out, 0));
        
        fvec_set_sample(res, fvec_get_sample(out, 0), i);
        //fvec_set_sample(res_time,((float)(i*hop_size))/((float)aubio_source_get_samplerate(s)), i);
        i++;
    }while (read==hop_size);
    
    //fvec_print(res_time);
    //del_fvec(res_time);
    del_aubio_pitch(pitch);
    del_fvec(in);
    del_fvec(out);
    aubio_cleanup();
    return res;
}

fvec_t* getPitch_fvec(fvec_t* src, uint_t win_size, uint_t start, uint_t hop_size,uint_t samplerate){
    aubio_pitch_t* pitch=new_aubio_pitch("default", win_size, hop_size, samplerate);
    uint_t i=0;
    fvec_t* out_pitch=new_fvec(1);
    fvec_t* in_buff,*out_res=new_fvec(win_size%hop_size==0?win_size/hop_size:win_size/hop_size+1);
    
    while ((i+1)*hop_size<win_size) {
        in_buff=getWin(src, hop_size, i);
        aubio_pitch_do(pitch, in_buff, out_pitch);
        fvec_set_sample(out_res, fvec_get_sample(out_pitch, 0), i);
        i++;
        del_fvec(in_buff);
    }
    return out_res;
}

fmat_t* getMfcc(aubio_source_t* s, int hop_size) {
    //mfcc parameters
    uint_t n_filters = 40; // number of filters
    uint_t n_coefs = 12; // number of coefficients
    smpl_t samplerate = aubio_source_get_samplerate(s); // samplerate
    
    
    int frame_number = 0,i;
    uint_t read = 0;
    
    //datastructures
    //hop_size = in questo caso dimensione del buffer rappresentante la finestra temporale
    fvec_t *inFft=new_fvec(hop_size); // input buffer for Fft
    cvec_t *inMfcc=new_cvec(hop_size);    // input buffer for Mfcc
    fvec_t *out=new_fvec(n_coefs); // output mfcc coefficients
    fmat_t* res;
    
    
    //set dimension of result
    if (aubio_source_get_duration(s)%hop_size!=0) {
        res=new_fmat(aubio_source_get_duration(s)/hop_size+1, n_coefs);
    }else{
        res=new_fmat(aubio_source_get_duration(s)/hop_size, n_coefs);
    }
    
    //utils for transformation
    aubio_fft_t* fft = new_aubio_fft(hop_size);
    aubio_mfcc_t* mfcc = new_aubio_mfcc(hop_size, n_filters, n_coefs, samplerate); //Creazione oggetto per il calcolo mfcc
    
    do{
        aubio_source_do(s, inFft, &read);
        aubio_fft_do(fft, inFft, inMfcc);
        aubio_mfcc_do(mfcc, inMfcc, out);
        if (frame_number>= res->length) {
            break;
        }
        for (i=0; i<n_coefs; i++) {
            fmat_set_sample(res, fvec_get_sample(out, i), frame_number, i);
        }
        //Prossimo frame
        frame_number++;
    } while (read == hop_size);
    
    //Cleanup
    del_aubio_mfcc(mfcc);
    del_fvec(inFft);
    del_cvec(inMfcc);
    del_fvec(out);
    aubio_cleanup();
    return res;
}

fvec_t* getEnergy(aubio_source_t* s,int hop_size){
    fvec_t* in,*out;
    u_int read,i,j=0;
    smpl_t sum;
    
    in=new_fvec(hop_size);
    if (aubio_source_get_duration(s)%hop_size!=0) {
        out=new_fvec(aubio_source_get_duration(s)/hop_size+1);
    }else{
        out=new_fvec(aubio_source_get_duration(s)/hop_size);
    }
    
    do{
        aubio_source_do(s, in, &read);
        sum=0;
        for (i=0; i<read; i++) {
            sum+=fvec_get_sample(in, i)*fvec_get_sample(in, i);
        }
        j++;
        fvec_set_sample(out, sum, j);
        //printf("time: %f\t%f\n",((float)(j*hop_size))/((float)aubio_source_get_samplerate(s)),sum);
    }while (read==hop_size);
    
    del_fvec(in);
    aubio_cleanup();
    return out;
}

