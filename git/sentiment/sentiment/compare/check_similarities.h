//
//  check_similarities.h
//  sentiment
//
//  Created by Daniele Galletti on 05/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef check_similarities_h
#define check_similarities_h

#include <stdio.h>
#include "aubio.h"
#include "feat_structure.h"
#include "dsp_utility.h"
#include "extractor.h"

struct frame_simil {
    smpl_t sommaDeltaEnergia;
    smpl_t sommaDeltaPitch;
    fvec_t* sommaDeltaMfcc;
};

typedef struct frame_simil* similarities_t;
/**
 Return the delta similarity between values of a frame (energy, pitch and mfccs) and a whole emotion
 \param energy value to compare
 \param pitch value to compare
 \param mfcc array of 12 to compare
 \param e emotion to check
 \param sim need to store delta similarity
 **/
void check_frame_similarity(smpl_t energy,smpl_t pitch,smpl_t* mfcc, emozione* e, struct frame_simil* sim);
/**
 Return a set of frame_simil for every frame of the file
 \param source_energy fvec of energy taken by source
 \param source_pitch fvec of pitch taken by source
 \param source_mfcc fmat of mfcc taken by source
 \param e emotion to check
 \return array of frame_simil for every frame of the file
 **/
double* detect_emotion_values(fvec_t* source_energy,fvec_t* source_pitch,fmat_t* source_mfcc,emozione* e);
/**
 Return an array of fmat, the first is energy percentage, the secondo pitch percentage and the third mfcc.
 Every matrix has 8 channels (percentage of each emotion) each with the frame number of the input file
 **/
fmat_t** sentiment_detector(char* uri,sentiment_t struttura);
/**
 Return 1 if the values are valid, 0 otherwise
 \param energy energy value
 \param pitch pitch value
 \param mfcc mfcc fvec
 \return 1 or 0
 **/
uint8_t is_valid(float energy,float pitch,smpl_t* mfcc);
#endif /* check_similarities_h */
