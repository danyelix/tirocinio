//
//  compare.h
//  sentiment
//
//  Created by Daniele Galletti on 29/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef compare_h
#define compare_h

#include <stdio.h>
#include "aubio.h"
#include "dsp_utility.h"
#include "math_utility.h"
#include "cut_and_filt.h"

void delta_fisso(fvec_t* i,fvec_t* c,fvec_t* ris);

void delta_intervallo_con_delta_minimo(fvec_t* i,fvec_t* c,fvec_t* ris);

#endif /* compare_h */
