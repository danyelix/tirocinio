//
//  compare.c
//  sentiment
//
//  Created by Daniele Galletti on 29/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "compare.h"

void delta_fisso(fvec_t* t,fvec_t* c,fvec_t* ris){
    uint_t min_length=t->length<c->length?t->length:c->length;
    
    for (int i=0; i<min_length; i++) {
        fvec_set_sample(ris, (fvec_get_sample(t, i)-fvec_get_sample(c, i))*(fvec_get_sample(t, i)-fvec_get_sample(c, i)), i);
    }
}

void delta_intervallo_con_delta_minimo(fvec_t* t,fvec_t* c,fvec_t* ris){
    fvec_t* piu_corto=t->length<c->length?t:c;
    fvec_t* altro=t->length<c->length?c:t;
    fvec_t* temp;
    fvec_t* thr;
    
    temp=fvec_distanzaQuadratica(fvec_get_sample(piu_corto, 0), altro);
    uint_t indice_inizio=fvec_indice_min(temp);
    
    del_fvec(temp);
    thr=get_threshold_until(altro, indice_inizio);
    
    temp=cutNfilt_fvec(altro, thr);
    del_fvec(thr);
    //thr ora è usata come seconda variabile temporanea
    thr=new_fvec(temp->length<piu_corto->length?temp->length:piu_corto->length);
    delta_fisso(piu_corto,temp , thr);
    *ris=*thr;
    del_fvec(temp);
}
