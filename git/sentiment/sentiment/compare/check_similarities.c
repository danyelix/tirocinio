//
//  check_similarities.c
//  sentiment
//
//  Created by Daniele Galletti on 05/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "check_similarities.h"


smpl_t fvec_minimo_noZero(fvec_t* t){
    int i;
    smpl_t temp,min=-1;
    //take the first non-zero element
    for (i=0; i<t->length; i++) {
        if (fvec_get_sample(t, i)!=0) {
            min=fvec_get_sample(t, i);
        }
    }
    
    
    for (i=0; i< t->length; i++) {
        temp=fvec_get_sample(t, i);
        if (temp!=0 && temp<min) {
            min=temp;
        }
    }
    return min;
}
//ritorna la riga minima senza calcolare le righe nulle
fvec_t* fmat_riga_minima_noZero(fmat_t* t){
    int i,j,indiceMinimo=0;
    smpl_t tempColonna,minimoColonna=0;
    fvec_t* minimo;
    //inizializza minimoColonna
    for (j=0; j<t->length; j++) {
        minimoColonna+=fmat_get_sample(t, 0, j);
    }
    
    
    for (i=1; i<t->height; i++) {
        //calcola la somma della colonna
        tempColonna=0;
        for(j=0;j<t->length;j++){
            tempColonna+=fmat_get_sample(t, i, j);
        }
        if (tempColonna==0) {
            continue;
        }
        //controlla il minimo
        if (tempColonna<minimoColonna || minimoColonna==0) {
            indiceMinimo=i;
            minimoColonna=tempColonna;
        }
    }
    
    minimo=new_fvec(t->length);
    for (j=0; j<minimo->length; j++) {
        fvec_set_sample(minimo, fmat_get_sample(t, indiceMinimo, j), j);
    }
    return minimo;
}

void check_frame_similarity(smpl_t energy,smpl_t pitch,smpl_t* mfcc, emozione* e,struct frame_simil* sim){
    int i,j;
    fvec_t* tempPitch;
    fvec_t* tempEnergy;
    fmat_t* tempMfcc;
    fvec_t* simil_pitch=new_fvec(e->length);
    fvec_t* simil_energy=new_fvec(e->length);
    fvec_t** simil_mfcc=malloc(e->length*sizeof(fvec_t*));
    
    smpl_t sommaDeltaEnergia=0;
    smpl_t sommaDeltaPitch=0;
    fvec_t* sommaDeltaMfcc=new_fvec(12);
    fvec_zeros(sommaDeltaMfcc);
    
    /*for (i=0; i<12; i++) {
        printf("%f\n",mfcc[i]);
    }
    printf("\n\n");*/
    
    for (i=0; i<e->length; i++) {
        tempPitch=fvec_distanzaQuadratica_noZero(pitch, e->pitches[i]);
        fvec_set_sample(simil_pitch, fvec_minimo_noZero(tempPitch), i);
        del_fvec(tempPitch);
        tempEnergy=fvec_distanzaQuadratica_noZero(energy, e->energies[i]);
        fvec_set_sample(simil_energy, fvec_minimo_noZero(tempEnergy), i);
        del_fvec(tempEnergy);
        tempMfcc=fmat_distanzaQuadratica_noJump(mfcc, e->mfccs[i]);
        simil_mfcc[i]=fmat_riga_minima_noZero(tempMfcc);
        del_fmat(tempMfcc);
    }
    
    for(i=0;i<e->length;i++){
        sommaDeltaEnergia+=fvec_get_sample(simil_energy, i);
        sommaDeltaPitch+=fvec_get_sample(simil_pitch, i);
        for (j=0; j<sommaDeltaMfcc->length; j++) {
            fvec_set_sample(sommaDeltaMfcc, fvec_get_sample(sommaDeltaMfcc, j)+fvec_get_sample(simil_mfcc[i], j), j);
        }
        del_fvec(simil_mfcc[i]);
    }
    del_fvec(simil_energy);
    del_fvec(simil_pitch);
    
    sim->sommaDeltaEnergia=sommaDeltaEnergia;
    sim->sommaDeltaPitch=sommaDeltaPitch;
    sim->sommaDeltaMfcc=sommaDeltaMfcc;
}

double* detect_emotion_values(fvec_t* source_energy,fvec_t* source_pitch,fmat_t* source_mfcc,emozione* e){
//    int i,k=0;
//    static similarities_t somiglianza_emozione;
//
//    somiglianza_emozione=malloc(source_energy->length*sizeof(struct frame_simil));
//
//    //iterates over all frames
//    for (i=0; i<source_energy->length; i++) {
//        //if (is_valid(fvec_get_sample(source_energy, i), fvec_get_sample(source_pitch, i), fmat_get_channel_data(source_mfcc, i))) {
//            check_frame_similarity(fvec_get_sample(source_energy, i), fvec_get_sample(source_pitch, i), fmat_get_channel_data(source_mfcc, i), e,&somiglianza_emozione[i]);
//            //k++;
//        //}
//    }
//    return somiglianza_emozione;
    double temp,temp_energy,temp_mfcc;
    //double min=2;
    double min_energy=0,min_pitch=0,min_mfcc=0,tot=0,tot_energy=0,tot_mfcc=0;
    
    int count=0,count_energy=0,count_mfcc=0;
    
    for (int i=0; i<e->length; i++) {
        //if (i==192) continue;
        temp = calculate_sliding_distance(source_pitch, e->pitches[i]);
        temp_energy = calculate_sliding_distance(source_energy, e->energies[i]);
        temp_mfcc = calculate_sliding_distance_fmat(source_mfcc, e->mfccs[i]);
        
        if (temp!=0.){    tot+=temp; count++;}
        if (temp_energy!=0.){    tot_energy+=temp; count_energy++;}
        if (temp_mfcc!=0.){    tot_mfcc+=temp; count_mfcc++;}
        
        if ((temp_energy<min_energy && temp_energy>0) || min_energy==0) {
            min_energy=temp_energy;
        }
        if ((temp<min_pitch && temp >0) || min_pitch==0) {
            min_pitch=temp;
        }
        if ((temp_mfcc<min_mfcc && temp_mfcc>0) || min_mfcc==0) {
            min_mfcc=temp_mfcc;
        }
        
    }
    
    tot/=count;
    tot_energy/=count_energy;
    tot_mfcc/=count_mfcc;
    
    //normalizes energy, mfcc distance
    min_energy*=pow(10, 20);
    min_mfcc*=pow(10, 10);
    
    tot_energy*=pow(10, 20);
    tot_mfcc*=pow(10, 10);
    
//    res = temp/(temp+temp_energy)*0.65 + temp_energy/(temp+temp_energy)*0.3;
//    if (res<min && res>0) min=res;
    
    double ciao[8]={min_pitch,min_energy,min_mfcc,tot,tot_energy,tot_mfcc};
    
    printf("distanza: %f    %f  %f  media: %f   %f  %f\n",min_pitch,min_energy,min_mfcc,tot,tot_energy,tot_mfcc);
    
//    FILE *f;
//    f = fopen("/Users/dany/Desktop/risultati_tests.txt", "a");
//    
//    if (f == NULL)
//    {
//        printf("Error opening file!\n");
//        exit(1);
//    }
//
//    fprintf(f, "%.17g;%.17g;%.17g;media;%.17g;%.17g;%.17g\n",min_pitch,min_energy,min_mfcc,tot,tot_energy,tot_mfcc);
//    
//    fclose(f);
    
    
    return ciao;
}

fmat_t** sentiment_detector(char* uri,sentiment_t struttura){
    int hop_size=512,i,j,k,numero_frames;
    aubio_source_t* source=new_aubio_source(uri, 0, hop_size);
    fvec_t* s_energy, *s_pitch;
    fmat_t* s_mfcc,*risultati_percentuali_energia,*risultati_percentuali_pitch,*risultati_percentuali_mfcc;
    similarities_t* detector;
    smpl_t tempSommaDeltaEnergy,tempSommaDeltaPitch,tempPercentuale;
    fvec_t* tempSommaDeltaMfcc=new_fvec(12);
    
    static fmat_t* risultato[3];
    
    detector=malloc(8*sizeof(similarities_t));
    
    s_energy=getEnergy(source, hop_size);
    aubio_source_close(source);
    source=new_aubio_source(uri, 0, hop_size);
    s_pitch=getPitch(source, hop_size);
    filter_fvec(s_pitch, 85, 300);
    aubio_source_close(source);
    source=new_aubio_source(uri, 0, hop_size);
    s_mfcc=getMfcc(source, hop_size);
    aubio_source_close(source);
    del_aubio_source(source);
    
    numero_frames=s_energy->length;
    
    risultati_percentuali_energia=new_fmat(8, numero_frames);
    risultati_percentuali_mfcc=new_fmat(8, numero_frames);
    risultati_percentuali_pitch=new_fmat(8, numero_frames);
    
    for (i=0; i<8; i++) {
        detector[i]=detect_emotion_values(s_energy, s_pitch, s_mfcc, &struttura[i]);
    }
    //trova la percentuale di emozioni in ogni frame
    for (i=0; i<numero_frames; i++) {
        tempSommaDeltaEnergy=0;
        tempSommaDeltaPitch=0;
        fvec_zeros(tempSommaDeltaMfcc);
        for (j=0; j<8; j++) {
            tempSommaDeltaEnergy+=detector[i][j].sommaDeltaEnergia;
            tempSommaDeltaPitch+=detector[i][j].sommaDeltaPitch;
            for (k=0; k<12; k++) {
                fvec_set_sample(tempSommaDeltaMfcc, fvec_get_sample(tempSommaDeltaMfcc, k)+fvec_get_sample(detector[j][i].sommaDeltaMfcc, k), k);
            }
        }
        for (j=0; j<8; j++) {
            tempPercentuale=(tempSommaDeltaEnergy - detector[j][i].sommaDeltaEnergia/tempSommaDeltaEnergy)*100;
            fmat_set_sample(risultati_percentuali_energia, tempPercentuale, j, i);
            tempPercentuale=(tempSommaDeltaPitch-detector[j][i].sommaDeltaPitch/tempSommaDeltaPitch)*100;
            fmat_set_sample(risultati_percentuali_pitch, tempPercentuale, j, i);
            tempPercentuale=0;
            for (k=0; k<12; k++) {
                tempPercentuale+=fvec_get_sample(detector[j][i].sommaDeltaMfcc, k)/fvec_get_sample(tempSommaDeltaMfcc, k);
            }
            tempPercentuale/=12;
            fmat_set_sample(risultati_percentuali_mfcc, tempPercentuale, j, i);
        }
        
    }
    
    for (i=0; i<8; i++) {
        free(detector[i]);
    }
    free(detector);
    del_fvec(s_energy);
    del_fvec(s_pitch);
    del_fmat(s_mfcc);
    
    risultato[0]=risultati_percentuali_energia;
    risultato[1]=risultati_percentuali_pitch;
    risultato[2]=risultati_percentuali_mfcc;
    return risultato;
}

uint8_t is_valid(float energy,float pitch,smpl_t* mfcc){
    return energy!=0 && pitch> 85 && pitch <300 && mfcc[0]!=-263.7279358;
}

double check_distance(float energy,float pitch,fvec_t* mfcc, float energy2, float pitch2, fvec_t* mfcc2){
    uint8_t i;
    double distanza;
    distanza=pow(energy2-energy,2)+pow(pitch2-pitch,2);
    for (i=0; i<12; i++) {
        distanza+=pow(fvec_get_sample(mfcc2, i)-fvec_get_sample(mfcc, i), 2);
    }
    return distanza;
}

