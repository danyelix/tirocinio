//
//  feat_structure.h
//  sentiment
//
//  Created by Daniele Galletti on 30/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef feat_structure_h
#define feat_structure_h

#include "aubio.h"


/*
 La sentiment_t è un array di emozione. Il tipo emozione è una struct che contiene array di fvec_t e fmat_t
 */
enum nomi_emozioni{
    neutral=0,
    calm=1,
    happy=2,//
    sad=3,//
    angry=4,//
    fearful=5,//
    disgust=6,//
    surprised=7//
};

enum tipo_dati{
    pitch=0,
    energy=1,
    mfcc=2
};

typedef struct e {
    uint_t length;
    uint_t pitch_ptr;
    fvec_t** pitches;
    uint_t energy_ptr;
    fvec_t** energies;
    uint_t mfcc_ptr;
    fmat_t** mfccs;
} emozione;

typedef emozione* sentiment_t;

//**************** FUNCTIONS *****************

/**
 Create a new sentiment_t type
 \return a new sentiment_t
 **/
sentiment_t new_sentiment(void);

/**
 Free all the allocated content of the sentiment_t
 \param s sentiment_t to delete
 **/
void del_sentiment(sentiment_t s);
/**
 Allocates the arrays for an emotion
 \param i emotion to allocate
 \param s the sentiment_t
 \param length the length of the emotion
 **/
void new_emozione(uint8_t i,sentiment_t s,uint_t length);

#endif /* feat_structure_h */
