//
//  feat_structure.c
//  sentiment
//
//  Created by Daniele Galletti on 30/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "feat_structure.h"

sentiment_t new_sentiment(){
    static emozione sentiment[8];
    static emozione e0;
    static emozione e1;
    static emozione e2;
    static emozione e3;
    static emozione e4;
    static emozione e5;
    static emozione e6;
    static emozione e7;
    
    sentiment[0]=e0;
    sentiment[1]=e1;
    sentiment[2]=e2;
    sentiment[3]=e3;
    sentiment[4]=e4;
    sentiment[5]=e5;
    sentiment[6]=e6;
    sentiment[7]=e7;
    
    
    return sentiment;
}

void new_emozione(uint8_t i,sentiment_t s,uint_t length){
    s[i].length=length;
    s[i].energy_ptr=0;
    s[i].pitch_ptr=0;
    s[i].mfcc_ptr=0;
    s[i].energies=(fvec_t**)malloc(s[i].length*sizeof(fvec_t*));
    s[i].pitches=(fvec_t**)malloc(s[i].length*sizeof(fvec_t*));
    s[i].mfccs=(fmat_t**)malloc(s[i].length*sizeof(fmat_t*));
}

void del_emozione(emozione e){
    int j;
    for (j=0; j<e.length; j++) {
        del_fvec(e.energies[j]);
        del_fvec(e.pitches[j]);
        del_fmat(e.mfccs[j]);
    }
    free(e.energies);
    free(e.pitches);
    free(e.mfccs);
}

void del_sentiment(sentiment_t s){
    int i;
    for (i=0; i<8; i++) {
        del_emozione(s[i]);
    }
}

