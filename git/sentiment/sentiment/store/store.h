//
//  store.h
//  sentiment
//
//  Created by Daniele Galletti on 30/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef store_h
#define store_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include "aubio.h"
#include "feat_structure.h"
#include "extractor.h"
#include "dsp_utility.h"
#include "math_utility.h"
#include "emotion_selector.h"

void store_single_Into_Structure(sentiment_t strutt, void* t,enum tipo_dati tipo,int8_t emozione);

#endif /* store */
