//
//  dataset_extractor.c
//  sentiment
//
//  Created by Daniele Galletti on 30/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "store.h"


void* allunga_array(void* pr, uint_t length_prima,size_t size,uint_t delta_length){
    static void* nuovo;
    nuovo=malloc(size*(length_prima+delta_length));
    memcpy(nuovo, pr, length_prima*size);
    free(pr);
    return nuovo;
}

void store_single_Into_Structure(sentiment_t strutt, void* t,enum tipo_dati tipo,int8_t emozione){
    switch (tipo) {
        case pitch:
            if (strutt[emozione].pitch_ptr>strutt[emozione].length) {
                strutt[emozione].pitches=(fvec_t**)allunga_array(strutt[emozione].pitches, strutt[emozione].length, sizeof(fvec_t*), strutt[emozione].pitch_ptr-strutt[emozione].length);
            }
            strutt[emozione].pitches[strutt[emozione].pitch_ptr]=(fvec_t*)t;
            strutt[emozione].pitch_ptr++;
            break;
        case energy:
            if (strutt[emozione].energy_ptr>strutt[emozione].length) {
                strutt[emozione].energies=(fvec_t**)allunga_array(strutt[emozione].energies, strutt[emozione].length, sizeof(fvec_t*), strutt[emozione].energy_ptr-strutt[emozione].length);
            }
            strutt[emozione].energies[strutt[emozione].energy_ptr]=(fvec_t*)t;
            strutt[emozione].energy_ptr++;
            break;
        case mfcc:
            if (strutt[emozione].mfcc_ptr>strutt[emozione].length) {
                strutt[emozione].mfccs=(fmat_t**)allunga_array(strutt[emozione].mfccs, strutt[emozione].length, sizeof(fvec_t*), strutt[emozione].mfcc_ptr-strutt[emozione].length);
            }
            strutt[emozione].mfccs[strutt[emozione].mfcc_ptr]=(fmat_t*)t;
            strutt[emozione].mfcc_ptr++;
            break;
        default:
            if (strutt[emozione].energy_ptr>strutt[emozione].length && strutt[emozione].pitch_ptr>strutt[emozione].length && strutt[emozione].mfcc_ptr>strutt[emozione].length) {
                strutt[emozione].length=min(strutt[emozione].energy_ptr, strutt[emozione].pitch_ptr, strutt[emozione].mfcc_ptr);
            }
            break;
    }
}

