//
//  function_filter.h
//  sentiment
//
//  Created by Daniele Galletti on 04/01/18.
//  Copyright © 2018 Daniele Galletti. All rights reserved.
//

#ifndef function_filter_h
#define function_filter_h

#include <stdio.h>
#include "aubio.h"
#include <math.h>

float trovaPrimaVoce(fvec_t* t);
fvec_t* trova_parlante(fvec_t* source);
float correggiF(fvec_t* t,int indice,float fAttuale,int giusti,int onARoll_length,fvec_t* v);
int get_nextIntervals(fvec_t* t,int indice);

#endif /* function_filter_h */
