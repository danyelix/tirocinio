//
//  cut_and_filt.c
//  sentiment
//
//  Created by Daniele Galletti on 21/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "cut_and_filt.h"

fvec_t* get_merged_threshold(fvec_t* p,fvec_t* e,fmat_t* m){
    fvec_t* pT=find_threshold(p, pitch_lb, pitch_ub);
    fvec_t* eT=find_threshold(e, energy_lb, energy_ub);
    
    fvec_weight(pT, eT);
    return pT;
}

fvec_t* cutNfilt_fvec(fvec_t* source,fvec_t* threshold){
    return cut_fvec_threshold(source, threshold);
}
fmat_t* cutNfilt_fmat(fmat_t* source,fvec_t* threshold){
    return cut_fmat_threshold(source, threshold);
}

fvec_t* get_threshold_until(fvec_t* p,uint_t index){
    fvec_t* ris=new_fvec(p->length);
    
    if (p->length/2>index) {
        fvec_ones(ris);
        for (int i=0; i<index; i++) {
            fvec_set_sample(ris, 0, i);
        }
    }else{
        fvec_zeros(ris);
        for (int i=index+1;i<p->length ; i++) {
            fvec_set_sample(ris, 1, i);
        }
    }
    return ris;
}

