//
//  cut_and_filt.h
//  sentiment
//  Offer an interface for low level filtering on pitch, energy and mfcc
//  Created by Daniele Galletti on 21/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef cut_and_filt_h
#define cut_and_filt_h

#define pitch_lb 85
#define pitch_ub 255
#define energy_lb 0
#define energy_ub -1

#include <stdio.h>
#include "aubio.h"
#include "filter.h"

fvec_t* get_merged_threshold(fvec_t* p,fvec_t* e,fmat_t* m);
fvec_t* get_threshold_until(fvec_t* p,uint_t index);
fvec_t* cutNfilt_fvec(fvec_t* source,fvec_t* threshold);
fmat_t* cufNfilt_fmat(fmat_t* source,fvec_t* threshold);

#endif /* cut_and_filt_h */
