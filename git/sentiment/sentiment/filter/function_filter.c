//
//  function_filter.c
//  sentiment
//
//  Created by Daniele Galletti on 04/01/18.
//  Copyright © 2018 Daniele Galletti. All rights reserved.
//

#include "function_filter.h"

const float intorno=25.;

fvec_t* trova_parlante(fvec_t* source){
    int minimaFUomo=85,rangeFUomo=95,minimaFDonna=165,rangeFDonna=90;
    
    
    
    return NULL;
}

uint8_t is_in_human_range(float t){
    return t<=255 &&t>=85;
}

uint8_t is_onARoll(fvec_t* t,int indice){
    int i=indice+1;
    float elementoAttuale=fvec_get_sample(t, indice);
    while (i<indice+5 && i<t->length) {
        if (fabs(fvec_get_sample(t, i)-elementoAttuale)>intorno) {
            return 0;
        }
        elementoAttuale=fvec_get_sample(t, i);
        i++;
    }
    return 1;
}

uint8_t is_Isolated(fvec_t* t, int indice){
    int contaSimili=0;
    float elementoIniziale=fvec_get_sample(t, indice);
    for (int i=indice; i<indice+25; i++) {
        if (fabs(fvec_get_sample(t, i)-elementoIniziale)<intorno) {
            contaSimili++;
        }
    }
    return contaSimili<10;
}

float trovaPrimaVoce(fvec_t* t){
    //trova prima voce umana parlante.. deve considerare anche i salti di uno
    uint_t i=0;
    uint_t found=0;
    while(!found && i<t->length){
        if (is_onARoll(t, i) && is_in_human_range(fvec_get_sample(t, i)) && !is_Isolated(t, i)) {
            //printf("\ninizio voce at: %f\n",((float)i)*256./44100.);
            found=i;
        }
        i++;
    }
    return fvec_get_sample(t, found);
}


float correggiF(fvec_t* t,int indice,float fAttuale,int giusti,int onARoll_length,fvec_t* v){
    float elementoAttuale=fvec_get_sample(t, indice);
    //passo base
    if (t->length-1==indice)
        if (fabs(fAttuale-elementoAttuale)<intorno && (is_in_human_range(elementoAttuale)) && (onARoll_length>0 ||is_onARoll(t, indice))){
            //media pesata con l'indice
            if (v!=NULL) {
                fvec_set_sample(v, 1, indice);
            }
            
            return fAttuale+ (elementoAttuale-fAttuale)/((float)giusti);
        }else{
            if (v!=NULL) {
                fvec_set_sample(v, 0, indice);
            }
            return fAttuale;
        }
    //passo ricorsivo
    if (fabs(fAttuale-elementoAttuale)<intorno && (is_in_human_range(elementoAttuale)) && (onARoll_length>0 ||is_onARoll(t, indice))) {
        //printf("time: %f    1\n",((float)indice)*256./44100.);
        //printf("pitch: %f\n",elementoAttuale);
        giusti+=fabs(fAttuale-elementoAttuale)<5.?0:1;
        if (v!=NULL) {
            fvec_set_sample(v, 1, indice);
        }
        return correggiF(t, indice+1, fAttuale + (elementoAttuale-fAttuale)/giusti,giusti,is_onARoll(t, indice)==1?5:onARoll_length-1,v);
    }
    //printf("time: %f    0\n",((float)indice)*256./44100.);
    if (v!=NULL) {
        fvec_set_sample(v, 0, indice);
    }
    return correggiF(t, indice+1, fAttuale,giusti,onARoll_length-1,v);
}


int get_nextIntervals(fvec_t* t,int indice){
    int i=indice,j=0;
    
    while (j<5){
        if (fvec_get_sample(t, i)!=0) {
            while(is_onARoll(t, i) && fvec_get_sample(t, i)!=0) {
                i+=5;
                j=0;
            }
        }
        j++;
        i++;
    }
    return i;
}
