//
//  filter.h
//  sentiment
//
//  Created by Daniele Galletti on 21/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef filter_h
#define filter_h

#include <stdio.h>
#include "aubio.h"
//************************** FILTER FUNCTIONS *******************************
/**
 Take a fvec and substitutes with 0 the corresponding values out of bounds
 \param input a fvec input
 \param lb lower bound
 \param ub upper bound
 **/
void filter_fvec(fvec_t* input,int lb,int ub );
/**
 Take an fvec and delete all the values out of the range between lb and ub
 \param input a fvec input
 \param lb lower bound of the range
 \param ub upper bound of the range if -1 no upper bound
 \return new cut fvec
 **/
fvec_t* cut_fvec(fvec_t* input, int lb, int ub);
/**
 Take a matrix and delete all the columns that contains the element in position equals to eq
 \param input an input fmat
 \param position in which the column must be checked
 \param eq the value to delete
 \return new cut fmat
 **/
fmat_t* cut_fmat(fmat_t* input, uint_t position, float eq);
/**
 Take a fvec_t and cut all the elements corresponding to 0 in the threshold fvec
 \param input a fvec of input
 \param threshold a fvec of threshold
 \return a fvec cut of the elements corresponding(same index) with 0 in the threshold fvec
 **/
fvec_t* cut_fvec_threshold(fvec_t* input,fvec_t* threshold);
/**
 Take an fmat and cut all the channels corresponding to a 0 in the threshold passed
 \param input an inputt fmat
 \param threshold fvec of threshold
 \return cut fmat
 **/
fmat_t* cut_fmat_threshold(fmat_t* input, fvec_t* threshold);
/**
 Return a threshold where elements equal to 1 are every element in input fvec between lb and ub and 0 otherwise
 \param input fvec to find threshold
 \param lb lower bound
 \param ub upper bound
 \return fvec threshold clculated on input
 **/
fvec_t* find_threshold(fvec_t* input,int lb, int ub);

#endif /* filter_h */
