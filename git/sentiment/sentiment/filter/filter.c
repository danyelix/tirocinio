//
//  filter.c
//  sentiment
//
//  Created by Daniele Galletti on 21/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "filter.h"

void filter_fvec(fvec_t* input,int lb,int ub ){
    uint_t i;
    fvec_t *filtro;
    
    filtro=new_fvec(input->length);
    for (i=0; i<input->length; i++) {
        if (ub<0) {
            fvec_set_sample(filtro, (fvec_get_sample(input, i)>lb)?1:0, i);
        }else{
            fvec_set_sample(filtro, (fvec_get_sample(input, i)>lb && fvec_get_sample(input, i)<ub)?1:0, i);
        }
    }
    fvec_weight(input, filtro);
    //del_fvec(filtro);
}

fvec_t* cut_fvec(fvec_t* input, int lb, int ub){
    uint_t i, contabuoni=0,j;
    fvec_t* tagliato;
    
    for (i=0; i<input->length; i++) {
        contabuoni+= ((ub==-1 && fvec_get_sample(input, i)>lb) || (fvec_get_sample(input, i)>lb && fvec_get_sample(input, i)<ub))?1:0;
    }
    if (contabuoni==0) {
        tagliato=new_fvec(1);
        fvec_set_sample(tagliato, 0, 0);
        return tagliato;
    }
    
    tagliato=new_fvec(contabuoni);
    j=0;
    for (i=0; i<input->length; i++) {
        if ((ub==-1 && fvec_get_sample(input, i)>lb) || (fvec_get_sample(input, i)>lb && fvec_get_sample(input, i)<ub)) {
            fvec_set_sample(tagliato, fvec_get_sample(input, i), j);
            j++;
        }
    }
    //del_fvec(input);
    return tagliato;
}

fmat_t* cut_fmat(fmat_t* input, uint_t position, float eq){
    uint_t i, contabuoni=input->height,j,k;
    fmat_t* tagliata;
    
    for (i=0; i<input->height; i++) {
        contabuoni-=fmat_get_sample(input, i, position)==eq?1:0;
    }
    if (contabuoni==input->height) {
        return input;
    }
    
    tagliata=new_fmat(contabuoni, input->length);
    k=0;
    for (i=0; i<input->height; i++) {
        if (fmat_get_sample(input, i, position)!=eq) {
            for (j=0; j<input->length; j++) {
                fmat_set_sample(tagliata, fmat_get_sample(input, i, j), k, j);
            }
            k++;
        }
    }
    //del_fmat(input);
    return tagliata;
}

fvec_t* find_threshold(fvec_t* input,int lb, int ub){
    fvec_t* t=new_fvec(input->length);
    fvec_zeros(t);
    for (int i=0; i<input->length; i++) {
        if (fvec_get_sample(input, i)>lb && (fvec_get_sample(input, i)<ub|| ub==-1)) {
            fvec_set_sample(t, 1, i);
        }
    }
    return t;
}

fvec_t* cut_fvec_threshold(fvec_t* input,fvec_t* threshold){
    fvec_weight(input, threshold);
    fvec_t* ris;
    ris=cut_fvec(input, 0, -1);
    return ris;
}

fmat_t* cut_fmat_threshold(fmat_t* input, fvec_t* threshold){
    //fmat_t* ris;
    for (int i=0; i<input->height; i++) {
        if (fvec_get_sample(threshold, i)==0) {
            fmat_set_sample(input, 0, i, 0);
        }
    }
    //ris=cut_fmat(input, 0, 0);
    return input;
}
