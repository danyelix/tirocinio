//
//  main.c
//  sentiment
//
//  Created by Daniele Galletti on 21/11/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include "aubio.h"
#include "fvec.h"
#include "io/source.h"
#include "extractor.h"
#include "dsp_utility.h"
#include "feat_structure.h"
#include "check_similarities.h"
#include "math_utility.h"
#include "filter.h"
#include "compare.h"
#include "load.h"
#include "function_filter.h"
#include "emotion_selector.h"
#include "store.h"

const char dataset1[]="/Users/dany/Documents/c/tirocinio/git/SpeechAO_AllActors/03-01-01-01-02-02-10.wav";
const char dataset2[]="/Users/dany/Documents/c/tirocinio/git/SpeechAO_AllActors/03-01-02-02-02-02-15.wav";
const char dataset5[]="/Users/dany/Documents/c/tirocinio/git/SpeechAO_AllActors/03-01-05-01-02-02-10.wav";
const char applause[]="/Users/dany/Documents/c/tirocinio/git/sentiment/Resources/Tests/Applause.wav";
const char hamlet_long_file[]="/Users/dany/Downloads/Hamlet - atto II - Lattore recita la morte di Priamo.wav";
const char tizio_lamentoso[]="/Users/dany/Desktop/dataset emotion speech/sadness/Dead\ Man\ Walking\ \(1995\)\ -\ Matthews\ Confession\ Scene\ \(611\)\ \ Movieclips.wav";
const char economiaRec[]="/Users/dany/Downloads/Vab Tir Cum Economia (online-audio-converter.com).wav";
const char miaVoce_conAltreVoci[]="/Users/dany/Downloads/Test Audio (online-audio-converter.com).wav";

const char robinWilliams[]= "/Users/dany/Desktop/dataset emotion speech/One Hour Photo (55) Movie CLIP - 48s.wav";
const char robinWilliams2[]= "/Users/dany/Desktop/dataset emotion speech/One Hour Photo (55) Movie CLIP - 42s.wav";
const char risata_kira[]="/Users/dany/Documents/c/tirocinio/audioDiTest/kira_risata.wav";
const char flight1[]="/Users/dany/Documents/c/tirocinio/audioDiTest/flight1lei.wav";
const char flight2[]="/Users/dany/Documents/c/tirocinio/audioDiTest/flight2lui.wav";
const char flight3[]="/Users/dany/Documents/c/tirocinio/audioDiTest/flight3lei.wav";
const char flight4[]="/Users/dany/Documents/c/tirocinio/audioDiTest/flight4lui.wav";
const char flight5[]="/Users/dany/Documents/c/tirocinio/audioDiTest/flight5lei.wav";

const char denzelino[]= "/Users/dany/Desktop/dataset emotion speech/Flight Courtroom Scene Denzel Washington Im an Alcoholic in Court.wav";

const char lei1[]="/Users/dany/Documents/c/tirocinio/audioDiTest/1_lei.wav";
const char lui2[]="/Users/dany/Documents/c/tirocinio/audioDiTest/2_lui.wav";
const char lei3[]="/Users/dany/Documents/c/tirocinio/audioDiTest/3_lei.wav";
const char lui4[]="/Users/dany/Documents/c/tirocinio/audioDiTest/4_lui.wav";
const char lui41[]="/Users/dany/Documents/c/tirocinio/audioDiTest/4_lui_prima.wav";
const char lui42[]="/Users/dany/Documents/c/tirocinio/audioDiTest/4_lui_seconda.wav";
const char lui422[]="/Users/dany/Documents/c/tirocinio/audioDiTest/4_lui_seconda_2.wav";
const char lui4eng[]="/Users/dany/Documents/c/tirocinio/audioDiTest/4_lui_eng.wav";
const char lei1eng[]="/Users/dany/Documents/c/tirocinio/audioDiTest/1_lei_eng.wav";
const char lui2eng[]="/Users/dany/Documents/c/tirocinio/audioDiTest/2_lui_eng.wav";
const char lei3eng[]="/Users/dany/Documents/c/tirocinio/audioDiTest/3_lei_eng.wav";
const char will1[]="/Users/dany/Documents/c/tirocinio/audioDiTest/willrisate.wav";

const char yoga1[]="/Users/dany/Documents/c/tirocinio/audioDiTest/yoga1.wav";
const char yoga2[]="/Users/dany/Documents/c/tirocinio/audioDiTest/yoga2.wav";

const char truman1[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman1.wav";
const char truman2[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman2.wav";
const char truman3[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman3.wav";
const char truman4[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman4.wav";
const char truman5[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman5.wav";
const char truman6[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman6.wav";
const char truman7[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman7.wav";
const char truman8[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman8.wav";
//const char truman1[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman1.wav";
//const char truman1[]="/Users/dany/Documents/c/tirocinio/audioDiTest/truman1.wav";
int main(int argc, char** argv){
//    aubio_source_t* file= loadFile( miaVoce_conAltreVoci, 256);
//    fvec_t* t=getPitch(file, 256);
//    fvec_t* thr=new_fvec(t->length);
//    float pitch_medio=correggiF(t, 0, trovaPrimaVoce(t), 1, 0, thr);
//    printf("pitch_medio: %f\n\n",pitch_medio);
//    fvec_weight(t, thr);
//    int i=0,intervallo;
//    while (i<t->length) {
//        while (fvec_get_sample(t, i)==0) {
//            i++;
//        }
//        intervallo=get_nextIntervals(t, i);
//        printf("int indice° %d-%d\n",i,intervallo);
//        i=intervallo;
//    }
//
//    del_aubio_source(file);
//    del_fvec(t);
//    del_fvec(thr);
    
    /*
    aubio_source_t* file= loadFile( miaVoce_conAltreVoci, 256);
    fvec_t* pitch=getPitch(file, 256);
    float pitch_medio=trovaPrimaVoce(pitch);
    fvec_t* thr=new_fvec(pitch->length);
    printf("pitch_iniziale: %f\n",pitch_medio);
    pitch_medio=correggiF(pitch, 0, pitch_medio,1,0,thr);
    printf("pitch medio: %f\n\n",pitch_medio);
    //print_fvec_with_time(pitch, 256./44100.);
    //print_fvec_with_time(thr, 256./44100.);
    del_aubio_source(file);
    del_fvec(pitch);
    del_fvec(thr);*/
    
    //default staff
    
    char* uri="/Users/dany/Documents/Documenti - MacBook Air di Daniele/c/tirocinio/git/SpeechAO_AllActors";
    fvec_t* source_pitch,*source_energy;
    fmat_t* source_mfcc;


    sentiment_t ciao= new_sentiment();

    populateWithDataset(uri,dataset_smartLaboratories,ciao);

    for (int i=0; i<8; i++) {
        printf("file caricati per emozione %d : %d\n",i, ciao[i].length);

    }
    int hop_size=512;
    const char* name=truman8;
    aubio_source_t* input= new_aubio_source(name, 0, hop_size);
    source_pitch= getPitch(input, hop_size);
    aubio_source_close(input);
    input=new_aubio_source(name, 0, hop_size);
    source_energy= getEnergy(input, hop_size);
    aubio_source_close(input);
    input=new_aubio_source(name, 0, hop_size);
    source_mfcc=getMfcc(input, hop_size);
    aubio_source_close(input);
    del_aubio_source(input);

    //do stuff

    //applico filtri
//    float pitch_medio= trovaPrimaVoce(source_pitch);
    fvec_t* thr=new_fvec(source_pitch->length);
    fvec_ones(thr);
//    pitch_medio=correggiF(source_pitch, 0, pitch_medio,1,0,thr);
    

    for (int i=0; i<source_pitch->length; i++) {
        if (fmat_get_sample(source_mfcc, i, 0) ==-263.7279358) {
            fvec_set_sample(thr, 0, i);
        }
        if (fvec_get_sample(source_energy, i) ==0) {
            fvec_set_sample(source_energy, 0, i);
        }
    }
    fvec_weight(source_pitch, thr);
    fvec_weight(source_energy, thr);
    source_mfcc=cut_fmat_threshold(source_mfcc, thr);


    FILE *f;
    f = fopen("/Users/dany/Desktop/risultati_tests.txt", "a");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }
    fprintf(f, "%s\n",name);
    
    double tot_pitch=0, tot_mfcc=0,tot_energy=0,min_pitch=0,min_energy=0,min_mfcc=0;
    double ris[8][6];
    
    double *temp;
    for (int i=0; i<8; i++) {
        temp=detect_emotion_values(source_energy, source_pitch, source_mfcc, &ciao[i]);
        min_pitch+=temp[0];
        min_energy+=temp[1];
        min_mfcc+=temp[2];
        tot_pitch+=temp[3];
        tot_energy+=temp[4];
        tot_mfcc+=temp[5];
        
        ris[i][0]=temp[0];
        ris[i][1]=temp[1];
        ris[i][2]=temp[2];
        ris[i][3]=temp[3];
        ris[i][4]=temp[4];
        ris[i][5]=temp[5];
        //fprintf(f, "%.17g;%.17g;%.17g;media;%.17g;%.17g;%.17g\n",min_pitch,min_energy,min_mfcc,tot_pitch,tot_energy,tot_mfcc);
    }
    
    for (int i=0; i<8; i++) {
        fprintf(f, "%f;%f;%f;media;%f;%f;%f\n",ris[i][0]/min_pitch*100,ris[i][1]/min_energy*100,ris[i][2]/min_mfcc*100,ris[i][3]/tot_pitch*100,ris[i][4]/tot_energy*100,ris[i][5]/tot_mfcc*100);
    }
    
    
    fclose(f);
    
    del_fvec(source_energy);
    del_fvec(source_pitch);
    del_fmat(source_mfcc);
    del_sentiment(ciao);
    return 0;
    
    

    name=truman6;
    input= new_aubio_source(name, 0, hop_size);
    source_pitch= getPitch(input, hop_size);
    aubio_source_close(input);
    input=new_aubio_source(name, 0, hop_size);
    source_energy= getEnergy(input, hop_size);
    aubio_source_close(input);
    input=new_aubio_source(name, 0, hop_size);
    source_mfcc=getMfcc(input, hop_size);
    aubio_source_close(input);
    del_aubio_source(input);
    
    //do stuff
    
    //applico filtri
    //    float pitch_medio= trovaPrimaVoce(source_pitch);
    thr=new_fvec(source_pitch->length);
    fvec_ones(thr);
    //    pitch_medio=correggiF(source_pitch, 0, pitch_medio,1,0,thr);
    
    
    for (int i=0; i<source_pitch->length; i++) {
        if (fmat_get_sample(source_mfcc, i, 0) ==-263.7279358) {
            fvec_set_sample(thr, 0, i);
        }
        if (fvec_get_sample(source_energy, i) ==0) {
            fvec_set_sample(source_energy, 0, i);
        }
    }
    fvec_weight(source_pitch, thr);
    fvec_weight(source_energy, thr);
    source_mfcc=cut_fmat_threshold(source_mfcc, thr);
    
    
    //FILE *f;
    f = fopen("/Users/dany/Desktop/risultati_tests.txt", "a");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }
    fprintf(f, "%s\n",name);
    
    tot_pitch=0; tot_mfcc=0;tot_energy=0;min_pitch=0;min_energy=0;min_mfcc=0;
    //double ris[8][6];
    
    //double *temp;
    for (int i=0; i<8; i++) {
        temp=detect_emotion_values(source_energy, source_pitch, source_mfcc, &ciao[i]);
        min_pitch+=temp[0];
        min_energy+=temp[1];
        min_mfcc+=temp[2];
        tot_pitch+=temp[3];
        tot_energy+=temp[4];
        tot_mfcc+=temp[5];
        
        ris[i][0]=temp[0];
        ris[i][1]=temp[1];
        ris[i][2]=temp[2];
        ris[i][3]=temp[3];
        ris[i][4]=temp[4];
        ris[i][5]=temp[5];
        //fprintf(f, "%.17g;%.17g;%.17g;media;%.17g;%.17g;%.17g\n",min_pitch,min_energy,min_mfcc,tot_pitch,tot_energy,tot_mfcc);
    }
    
    for (int i=0; i<8; i++) {
        fprintf(f, "%f;%f;%f;media;%f;%f;%f\n",ris[i][0]/min_pitch*100,ris[i][1]/min_energy*100,ris[i][2]/min_mfcc*100,ris[i][3]/tot_pitch*100,ris[i][4]/tot_energy*100,ris[i][5]/tot_mfcc*100);
    }
    
    
    fclose(f);
    
    del_fvec(source_energy);
    del_fvec(source_pitch);
    del_fmat(source_mfcc);
    del_sentiment(ciao);
    return 0;
    
    name=truman7;
    input= new_aubio_source(name, 0, hop_size);
    source_pitch= getPitch(input, hop_size);
    aubio_source_close(input);
    input=new_aubio_source(name, 0, hop_size);
    source_energy= getEnergy(input, hop_size);
    aubio_source_close(input);
    input=new_aubio_source(name, 0, hop_size);
    source_mfcc=getMfcc(input, hop_size);
    aubio_source_close(input);
    del_aubio_source(input);
    
    //do stuff
    
    //applico filtri
    //    float pitch_medio= trovaPrimaVoce(source_pitch);
    thr=new_fvec(source_pitch->length);
    fvec_ones(thr);
    //    pitch_medio=correggiF(source_pitch, 0, pitch_medio,1,0,thr);
    
    
    for (int i=0; i<source_pitch->length; i++) {
        if (fmat_get_sample(source_mfcc, i, 0) ==-263.7279358) {
            fvec_set_sample(thr, 0, i);
        }
        if (fvec_get_sample(source_energy, i) ==0) {
            fvec_set_sample(source_energy, 0, i);
        }
    }
    fvec_weight(source_pitch, thr);
    fvec_weight(source_energy, thr);
    source_mfcc=cut_fmat_threshold(source_mfcc, thr);
    
    
    //FILE *f;
    f = fopen("/Users/dany/Desktop/risultati_tests.txt", "a");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }
    fprintf(f, "%s\n",name);
    
    tot_pitch=0; tot_mfcc=0;tot_energy=0;min_pitch=0;min_energy=0;min_mfcc=0;
    //double ris[8][6];
    
    //double *temp;
    for (int i=0; i<8; i++) {
        temp=detect_emotion_values(source_energy, source_pitch, source_mfcc, &ciao[i]);
        min_pitch+=temp[0];
        min_energy+=temp[1];
        min_mfcc+=temp[2];
        tot_pitch+=temp[3];
        tot_energy+=temp[4];
        tot_mfcc+=temp[5];
        
        ris[i][0]=temp[0];
        ris[i][1]=temp[1];
        ris[i][2]=temp[2];
        ris[i][3]=temp[3];
        ris[i][4]=temp[4];
        ris[i][5]=temp[5];
        //fprintf(f, "%.17g;%.17g;%.17g;media;%.17g;%.17g;%.17g\n",min_pitch,min_energy,min_mfcc,tot_pitch,tot_energy,tot_mfcc);
    }
    
    for (int i=0; i<8; i++) {
        fprintf(f, "%f;%f;%f;media;%f;%f;%f\n",ris[i][0]/min_pitch*100,ris[i][1]/min_energy*100,ris[i][2]/min_mfcc*100,ris[i][3]/tot_pitch*100,ris[i][4]/tot_energy*100,ris[i][5]/tot_mfcc*100);
    }
    
    
    fclose(f);
    

    del_fvec(source_energy);
    del_fvec(source_pitch);
    del_fmat(source_mfcc);
    del_sentiment(ciao);
    
    
//    fvec_t* ciao =new_fvec(10);
//    fvec_set_sample(ciao, 0, 0);
//    fvec_set_sample(ciao, 1, 1);
//    fvec_set_sample(ciao, 2, 2);
//    fvec_set_sample(ciao, 3, 3);
//    fvec_set_sample(ciao, 4, 4);
//    fvec_set_sample(ciao, 5, 5);
//    fvec_set_sample(ciao, 6, 6);
//    fvec_set_sample(ciao, 7, 7);
//    fvec_set_sample(ciao, 8, 8);
//    fvec_set_sample(ciao, 9, 9);
//
//
//    printf("%f",calculate_sliding_distance(ciao, ciao));
}
