//
//  emotion_selector.c
//  sentiment
//
//  Created by Daniele Galletti on 22/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "emotion_selector.h"

int8_t dataset_smartLaboratories(char* filename){
    switch (filename[7]-'0') {
        case 1:
            //neutral
            return neutral;
        case 2:
            return calm;
        case 3:
            return happy;
        case 4:
            return sad;
        case 5:
            return angry;
        case 6:
            return fearful;
        case 7:
            return disgust;
        case 8:
            return surprised;
        default:
            return -1;
    }
}

int8_t* dataset_emotion_index(char** t){
    return -1;
}
