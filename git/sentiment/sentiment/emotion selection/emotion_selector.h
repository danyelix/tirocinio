//
//  emotion_selector.h
//  sentiment
//
//  Created by Daniele Galletti on 22/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef emotion_selector_h
#define emotion_selector_h

#include <stdio.h>
#include "feat_structure.h"

/**
 Schedule function that works with dataset from Smart Laboratories
 \return integer between 0 and 7, if the file is not matched return -1
 **/
int8_t dataset_smartLaboratories(char* filename);

int8_t* dataset_emotion_index(char** t);
#endif /* emotion_selector_h */
