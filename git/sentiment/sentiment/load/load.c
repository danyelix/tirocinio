//
//  load.c
//  sentiment
//
//  Created by Daniele Galletti on 21/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#include "load.h"
aubio_source_t* loadFile(const char* uri,int hop_size){
    return new_aubio_source(uri, 0, hop_size);
}

struct file* loadDir(const char* uri,int hop_size){
    DIR* dir;
    struct dirent* ent;
    static struct file* ris;
    int numFile=0,i=0;
    char nomeTemp[1024];
    
    
    dir=opendir(uri);

    
    //conta le occorrenze dei file audio
    while((ent = readdir (dir)) != NULL){
        if (strstr(ent->d_name, ".wav")) {
            numFile++;
        }
    }
    closedir(dir);
    
    dir=opendir(uri);
    
    ris=(struct file*)malloc(numFile*sizeof(struct file));
    
    while ((ent = readdir (dir)) != NULL) {
        if (!strstr(ent->d_name, ".wav"))   continue;
        nomeTemp[0] = '\0';
        ris[i].info =loadFile(strcat(strcat(strcat(nomeTemp,uri),"/"),ent->d_name), hop_size);
        ris[i].name = ent->d_name;
    }
    closedir(dir);
    return ris;
}

char** get_all_names(const char* uri){
    DIR* dir;
    struct dirent* ent;
    char** ris;
    int numFile=0,i=0;
    char nomeTemp[1024];
    
    
    dir=opendir(uri);
    
    
    //conta le occorrenze dei file audio
    while((ent = readdir (dir)) != NULL){
        if (strstr(ent->d_name, ".wav")) {
            numFile++;
        }
    }
    
    ris=(char**)malloc(numFile*sizeof(char*));
    for (int i=0; i<numFile; i++) {
        ris[i]=malloc(1024*sizeof(char));
    }
    
    while ((ent = readdir (dir)) != NULL) {
        strcat(strcat(strcat(nomeTemp,uri),"/"),(ent->d_name));
        memcpy(ris[i], nomeTemp, 1024*sizeof(char));
    }
    closedir(dir);
    return ris;
}

int count(const char* uri, const char* prefix){
    
    DIR* dir;
    struct dirent* ent;
    int numFile=0;
    
    dir=opendir(uri);
    
    while((ent = readdir (dir)) != NULL){
        if (strstr(ent->d_name, ".wav") && strstr(ent->d_name, prefix)) {
            numFile++;
        }
    }
    
    closedir(dir);
    return numFile;
}

void del_dir(struct file* ciao,int size){
    for (int i=0; i<size; i++) {
        if (ciao[i].info!=NULL)
            del_aubio_source(ciao[i].info);
    }
    free(ciao);
}

void populateWithDataset(char* directory,int8_t (*f)(char* filename),sentiment_t s){
    DIR* dir;
    FILE* output;
    struct dirent* ent;
    aubio_source_t* temp;
    int hop_size=512,i,numeroFile[8]={0,0,0,0,0,0,0,0},indexFile[8]={0,0,0,0,0,0,0,0};
    emozione* emozione_attuale;
    char nomeTemp[1024];
    
    fvec_t *tempEnergy,*tempPitch;
    fmat_t *tempMfcc;
    
    output=fopen("/Users/dany/Desktop/extractedData.txt", "a+");
    dir=opendir(directory);
    
    //conta le occorrenze dei file associati ad emozione
    while((ent = readdir (dir)) != NULL){
        if (f(ent->d_name)==9) {
            continue;
        }else{
            numeroFile[dataset_smartLaboratories(ent->d_name)]++;
        }
    }
    
    //inizializza struttura emozione
    for (i=0; i<8; i++) {
        s[i].length=numeroFile[i];
        s[i].energies=(fvec_t**)malloc(s[i].length*sizeof(fvec_t*));
        s[i].pitches=(fvec_t**)malloc(s[i].length*sizeof(fvec_t*));
        s[i].mfccs=(fmat_t**)malloc(s[i].length*sizeof(fmat_t*));
    }
    
    closedir(dir);
    dir=opendir(directory);
    int ind=0;
    while ((ent = readdir (dir)) != NULL) {
        
        nomeTemp[0]=0;
        if(!strstr(ent->d_name, ".wav")){
            continue;
        }
        emozione_attuale=&s[f(ent->d_name)];
        strcat(strcat(strcat(nomeTemp,directory),"/"),(ent->d_name));
        temp=new_aubio_source(nomeTemp, 0, hop_size);
        //se il file non viene letto da aubio passa oltre
        if (aubio_source_get_duration(temp)==0 || f(ent->d_name)==-1) {
            continue;
        }
        
        
        
        tempEnergy=getEnergy(temp, hop_size);
        //emozione_attuale->energies[indexFile[f(ent->d_name)]]=tempEnergy;
        aubio_source_close(temp);
        temp=new_aubio_source(nomeTemp, 0, hop_size);
        tempMfcc=getMfcc(temp, hop_size);
        //emozione_attuale->mfccs[indexFile[f(ent->d_name)]]=tempMfcc;
        aubio_source_close(temp);
        temp=new_aubio_source(nomeTemp, 0, hop_size);
        tempPitch=getPitch(temp, hop_size);
        //emozione_attuale->pitches[indexFile[f(ent->d_name)]]=tempPitch;
        aubio_source_close(temp);
        
        temp=new_aubio_source(nomeTemp, 0, hop_size);
        //int g= tempPitch->length;
        for(int j=0;j<tempPitch->length;j++){
            float tempo=((float)aubio_source_get_duration(temp))/aubio_source_get_samplerate(temp)/hop_size*j;
            
            fprintf(output, "%d;%s;%d;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%d\n",ind,ent->d_name,j,tempo,fvec_get_sample(tempPitch, j),fmat_get_sample(tempMfcc, j, 0),fmat_get_sample(tempMfcc, j, 1),fmat_get_sample(tempMfcc, j, 2),fmat_get_sample(tempMfcc, j, 3),fmat_get_sample(tempMfcc, j, 4),fmat_get_sample(tempMfcc, j, 5),fmat_get_sample(tempMfcc, j, 6),fmat_get_sample(tempMfcc, j, 7),fmat_get_sample(tempMfcc, j, 8),fmat_get_sample(tempMfcc, j, 9),fmat_get_sample(tempMfcc, j, 10),fmat_get_sample(tempMfcc, j, 11),fvec_get_sample(tempEnergy, j),f(ent->d_name));
        }
        
        fprintf(output, "\n");
        
        //filtraggio
//        float pitch_medio= trovaPrimaVoce(tempPitch);
        fvec_t* thr=new_fvec(tempPitch->length);
        fvec_ones(thr);
//        pitch_medio=correggiF(tempPitch, 0, pitch_medio,1,0,thr);
        
        for (int i=0; i<tempPitch->length; i++) {
            if (fmat_get_sample(tempMfcc, i, 0) ==-263.7279358) {
                fvec_set_sample(thr, 0, i);
            }
            if (fvec_get_sample(tempEnergy, i) ==0) {
                fvec_set_sample(tempEnergy, 0, i);
            }
        }
        fvec_weight(tempPitch, thr);
        fvec_weight(tempEnergy, thr);
        tempMfcc= cut_fmat_threshold(tempMfcc,thr);
        
        indexFile[f(ent->d_name)]++;
        ind++;
    }
    closedir(dir);
    fclose(output);
    del_aubio_source(temp);
}
