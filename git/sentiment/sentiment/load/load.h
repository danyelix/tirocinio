//
//  load.h
//  sentiment
//
//  Created by Daniele Galletti on 21/12/17.
//  Copyright © 2017 Daniele Galletti. All rights reserved.
//

#ifndef load_h
#define load_h

#include <stdio.h>
#include <stdlib.h>
#include "aubio.h"
#include <dirent.h>
#include <string.h>
#include "feat_structure.h"
#include "extractor.h"
#include "function_filter.h"

struct file{
    aubio_source_t* info;
    char* name;
};

aubio_source_t* loadFile(const char* uri,int hop_size);
struct file* loadDir(const char* uri,int hop_size);
char** get_all_names(const char* uri);
int count(const char* uri, const char* prefix);
void del_dir(struct file* ciao,int size);
void populateWithDataset(char* directory,int8_t (*f)(char* filename),sentiment_t s);
#endif /* load_h */
